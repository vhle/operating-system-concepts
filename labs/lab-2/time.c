/* Measure the running time of an application */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>

void help(char *prog_name)
{
	printf("Usage:\n");
	printf("\t%s <program> [args]\n", prog_name);
}

double get_time_diff(struct timeval *start, struct timeval *end)
{
	double secs = end->tv_sec - start->tv_sec;
	double usecs = end->tv_usec - start->tv_usec;
	return secs + usecs / 1000000000.0;
}

int main(int argc, char **argv)
{
	struct timeval start_time, end_time;
	gettimeofday(&start_time, NULL);

	pid_t pid = fork();
	if (pid == -1) {
		perror("fork() error\n");
		exit(1);
	} else if (pid == 0) {
		if (argv[1] == NULL) {
			help(argv[0]);
			exit(1);
		} else {
			++argv;
			execvp(argv[0], argv);
		}
	} else {
		wait(NULL);
		gettimeofday(&end_time, NULL);
		double timediff = get_time_diff(&start_time, &end_time);
		printf("Elapsed time: %.2fs\n", timediff);
	}

	return 0;
}
