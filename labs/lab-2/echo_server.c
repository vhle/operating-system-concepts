/**
 * Single-threaded echo server.
 * Usage:
 *      ./echo_server <port>
 * Test client: nc localhost <port>
 */

#include <asm-generic/socket.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

void swap_case(char *buf, int size)
{
	for (int i = 0; i < size; i++) {
		if (islower(buf[i])) {
			buf[i] = toupper(buf[i]);
		} else {
			buf[i] = tolower(buf[i]);
		}
	}
}

void sercer_thread_func(int server_fd)
{
	struct sockaddr_in client;
	socklen_t client_len = sizeof(client);
	int client_fd;
	char buf[BUFFER_SIZE];

	client_fd = accept(server_fd, (struct sockaddr *)&client, &client_len);
	if (client_fd == -1) {
		perror("accept");
		exit(1);
	}

	while (1) {
		int read = recv(client_fd, buf, BUFFER_SIZE, 0);
		if (read == 0) { // Done reading
			break;
		} else if (read == -1) {
			perror("read error");
			exit(1);
		}
		swap_case(buf, read);
		if (send(client_fd, buf, read, 0) == -1) {
			perror("write error");
			exit(1);
		}
	}
}

int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("Usage:\n\t%s <port>\n", argv[0]);
		exit(1);
	}
	int port = atoi(argv[1]);

	int server_fd;
	struct sockaddr_in server;

	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd < 0) {
		perror("Error creating socket");
		exit(1);
	}

	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	int opt_val = 1;
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val,
		       sizeof(opt_val)) == -1) {
		perror("setsockopt");
		exit(1);
	}

	if (bind(server_fd, (struct sockaddr *)&server, sizeof(server)) == -1) {
		perror("bind");
		exit(1);
	}

	if (listen(server_fd, 0) == -1) {
		perror("listen");
		exit(1);
	}

	struct sockaddr_in client;
	socklen_t client_len = sizeof(client);
	int client_fd;
	char buf[BUFFER_SIZE];
	while (1) {
		client_fd = accept(server_fd, (struct sockaddr *)&client,
				   &client_len);
		if (client_fd == -1) {
			perror("accept");
			exit(1);
		}

		while (1) {
			int read = recv(client_fd, buf, BUFFER_SIZE, 0);
			if (read == 0) { // Done reading
				break;
			} else if (read == -1) {
				perror("read error");
				exit(1);
			}
			swap_case(buf, read);
			if (send(client_fd, buf, read, 0) == -1) {
				perror("write error");
				exit(1);
			}
		}
	}

	return 0;
}
