/* Create a zombie process */

#include <signal.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define SLEEP_TIME 10

static void wait_after_child_exit(int signum)
{
	const char message[] = "Signal received; wait 10 more seconds.\n";
	write(STDOUT_FILENO, message, sizeof(message));
	sleep(SLEEP_TIME);
	exit(0);
}

int main()
{
	pid_t pid;
	struct sigaction sa;
	sa.sa_handler = wait_after_child_exit;
	sigset_t sig;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	pid = fork();
	if (pid < 0) {
		fprintf(stderr, "Error Forking\n");
		exit(1);
	} else if (pid == 0) {
		printf("To become a zombie: %d\n", getpid());
		_exit(0);
	} else {
		printf("Waiting for child (%d) to finished + %ds.\n", pid,
		       SLEEP_TIME);
		fflush(stdout);
		pause(); /* Wait for signal */
		printf("Signal received.");
	}

	return 0;
}
