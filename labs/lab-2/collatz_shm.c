#include <fcntl.h>
#include <sched.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>

void help(char *program_name)
{
	printf("Usage:\n\t%s <positive integer>.\n", program_name);
}

#define MAX_SEQUENCE_LENGTH 1000

int main(int argc, char *argv[])
{
	if (argc != 2) {
		help(argv[0]);
		exit(1);
	}
	int n = atoi(argv[1]);
	if (n <= 0) {
		help(argv[0]);
		exit(1);
	}

	const char *name = "collatzshm";
	int fd = shm_open(name, O_CREAT | O_RDWR, 0600);
	if (fd == -1) {
		perror("Failed to open shared memory.");
		exit(1);
	}
	if (ftruncate(fd, MAX_SEQUENCE_LENGTH * sizeof(int)) == -1) {
		perror("ftruncate");
		exit(1);
	}

	pid_t child_pid = fork();
	ssize_t SHARED_ELEMENTS = 1 + MAX_SEQUENCE_LENGTH;
	if (child_pid == 0) {
		int *output = mmap(0, SHARED_ELEMENTS * sizeof(int),
				   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (output == MAP_FAILED) {
			perror("Child");
			_exit(-1);
		}
		output[0] = 0;
		int i = 0;
		pid_t pid = getpid();
		while (i < SHARED_ELEMENTS) {
			output[++i] = n;
			printf("(child) %d: %d\n", i, n);
			if (n <= 1) {
				break;
			}
			n = (n & 1) ? (3 * n + 1) : (n >> 1);
		};
		output[0] = i;
		_exit(0);
	} else {
		int *input = mmap(0, MAX_SEQUENCE_LENGTH * sizeof(int),
				  PROT_READ, MAP_SHARED, fd, 0);
		if (input == MAP_FAILED) {
			perror("Parent");
			exit(1);
		}
		int child_status;
		wait(&child_status);
		if (child_status) {
			fprintf(stderr, "Child exited with error code %d.\n",
				child_status);
			shm_unlink(name);
			exit(1);
		}
		int n_elements = input[0];
		for (int i = 1; i <= n_elements; i++) {
			printf("(parent): %d\n", input[i]);
		}
		shm_unlink(name);
	}
	return 0;
}
