#pragma once

#include <limits.h>
#include <stdbool.h>

#define BIT (CHAR_BITS * sizeof(byte))

void bitmap_allocate(char **bitmap, int size);
void bitmap_set(char *bitmap, int index);
void bitmap_clear(char *bitmap, int index);
bool bitmap_get(char *bitmap, int index);
