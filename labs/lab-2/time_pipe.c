/* Measure the running time of an application. Also using a pipe */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>

void help(char *prog_name)
{
	printf("Usage:\n");
	printf("\t%s <program> [args]\n", prog_name);
}

double get_time_diff(struct timeval *start, struct timeval *end)
{
	double secs = end->tv_sec - start->tv_sec;
	double usecs = end->tv_usec - start->tv_usec;
	return secs + usecs / 1000000000.0;
}
#define MESSAGE_SIZE 100

int main(int argc, char **argv)
{
	char write_msg[MESSAGE_SIZE];
	char read_msg[MESSAGE_SIZE];
	int fd[2];
	pid_t pid;

	if (pipe(fd) == -1) {
		perror("pipe error.\n");
		exit(1);
	}

	pid = fork();
	if (pid == -1) {
		perror("fork() error\n");
		exit(1);
	} else if (pid == 0) { /* Child */
		close(fd[0]);

		/* Write the start time to pipe */
		struct timeval start_time, end_time;
		gettimeofday(&start_time, NULL);
		sprintf(write_msg, "%ld %ld", start_time.tv_sec,
			start_time.tv_usec);
		write(fd[1], write_msg, strlen(write_msg) + 1);
		close(fd[1]);

		if (argv[1] == NULL) {
			help(argv[0]);
			exit(1);
		} else {
			++argv;
			execvp(argv[0], argv);
		}
	} else {
		close(fd[1]);
		/* Read the child's message */
		int rv = read(fd[0], read_msg, MESSAGE_SIZE);
		if (rv < 0) {
			perror("Cannot read data from pipe.");
			exit(1);
		}
		struct timeval start_time;
		rv = sscanf(read_msg, "%ld %ld", &start_time.tv_sec,
			    &start_time.tv_usec);
		if (rv == EOF) {
			perror("Cannot read message from pipe.");
			exit(1);
		}
		wait(NULL);

		struct timeval end_time;
		gettimeofday(&end_time, NULL);
		double timediff = get_time_diff(&start_time, &end_time);
		printf("Elapsed time: %.2fs\n", timediff);
	}

	return 0;
}
