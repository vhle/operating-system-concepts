#include "bitmap.h"

#include <limits.h>
#include <stdlib.h>

void bitmap_allocate(char **bitmap, int size)
{
	*bitmap = malloc((size + CHAR_BIT - 1) / CHAR_BIT);
}

void bitmap_set(char *bitmap, int index)
{
	int byte_offset = index / CHAR_BIT;
	int bit_offset = index % CHAR_BIT;
	bitmap[byte_offset] |= (1 << bit_offset);
}

void bitmap_clear(char *bitmap, int index)
{
	int byte_offset = index / CHAR_BIT;
	int bit_offset = index % CHAR_BIT;
	bitmap[byte_offset] &= ~(1 << bit_offset);
}

bool bitmap_get(char *bitmap, int index)
{
	int byte_offset = index / CHAR_BIT;
	int bit_offset = index % CHAR_BIT;
	return (bitmap[byte_offset] >> bit_offset) & true;
}
