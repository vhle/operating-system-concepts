#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
	const int SIZE = 4096;
	const char *name = "OS";
	const char *message_0 = "Hello";
	const char *message_1 = "World!";
	int fd;
	char *ptr;

	if ((fd = shm_open(name, O_CREAT | O_RDWR, 0666)) == -1) {
		fprintf(stderr, "Error opening shared memory.\n");
		exit(1);
	}
	if (ftruncate(fd, SIZE) == -1) {
		fprintf(stderr, "Error ftruncate\n");
		exit(1);
	}

	ptr = (char *)mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED) {
		fprintf(stderr, "mmap failed");
		exit(1);
	}

	sprintf(ptr, "%s", message_0);
	ptr += strlen(message_0);
	sprintf(ptr, "%s", message_1);
	ptr += strlen(message_1);

	printf("Producer: Success\n");
	return 0;
}
