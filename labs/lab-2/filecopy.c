#include <signal.h>
#include <stddef.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

void help(const char *program_name)
{
	printf("Usage:\n\t%s <source> <dest>\n", program_name);
}

#define BUFFER_SIZE 1024

void on_child_sigpipe(int signal)
{
	char message[] = "Child process: SIGPIPE\n";
	if (signal == SIGPIPE) {
		write(STDERR_FILENO, message, sizeof(message));
		exit(1);
	}
}

void on_parent_sigpipe(int signal)
{
	char message[] = "Parent process: SIGPIPE\n";
	if (signal == SIGPIPE) {
		write(STDERR_FILENO, message, sizeof(message));
		exit(1);
	}
}

int main(int argc, char **argv)
{
	if (argc != 3) {
		help(argv[0]);
		exit(1);
	}

	int fd[2];
	if (pipe(fd) == -1) {
		perror("pipe");
		exit(1);
	}

	unsigned char buffer[BUFFER_SIZE];
	pid_t pid = fork();
	if (pid < 0) {
		perror("fork");
		exit(1);
	} else if (pid == 0) {
		signal(SIGPIPE, on_child_sigpipe);
		close(fd[1]);
		FILE *output_file = fopen(argv[2], "w");
		if (output_file == NULL) {
			close(fd[0]);
			perror("Cannot open output file");
			exit(1);
		}
		int finished = 0;
		while (!finished) {
			size_t read_bytes = read(fd[0], buffer, BUFFER_SIZE);
			if (read_bytes < 0) {
				close(fd[0]);
				perror("Read pipe");
				exit(1);
			}
			finished = read_bytes < BUFFER_SIZE;
			if (fwrite(buffer, sizeof(unsigned char), read_bytes,
				   output_file) < read_bytes) {
				close(fd[0]);
				perror("Write to output file");
				exit(1);
			}
		}
	} else {
		signal(SIGPIPE, on_parent_sigpipe);
		close(fd[0]);
		FILE *input_file = fopen(argv[1], "r");
		if (input_file == NULL) {
			perror("Cannot open input file");
			close(fd[1]);
			exit(1);
		}
		int finished = 0;
		while (!finished) {
			size_t read = fread(buffer, sizeof(unsigned char),
					    BUFFER_SIZE, input_file);
			finished = read < BUFFER_SIZE;
			if (finished && ferror(input_file)) {
				close(fd[1]);
				perror("Error reading input file");
				exit(1);
			}
			if (write(fd[1], buffer, read) < read) {
				close(fd[1]);
				perror("Error writing to pipe");
				exit(1);
			}
		}
		wait(NULL);
	}
}
