#include <stdbool.h>
#include <stddef.h>
#include <assert.h>

#define MIN_PID 300
#define MAX_PID 5000
#define END_PID (MAX_PID + 1)

static struct pid_map_t {
	bool is_pid_used[END_PID - MIN_PID];
	int available_pids[END_PID - MIN_PID];
	int num_available_pids;

} pid_map;

/** Create and initialize a data structure for representing pids */
int allocate_map(void)
{
	for (int i = 0; i < END_PID - MIN_PID; i++) {
		pid_map.is_pid_used[i] = false;
		pid_map.available_pids[i] = i + MIN_PID;
	}
	pid_map.num_available_pids = END_PID - MIN_PID;
	return 1;
}

/** Allocate a PID */
int allocate_pid(void)
{
	if (pid_map.num_available_pids == 0) {
		return -1;
	}

	pid_map.num_available_pids--;
	pid_map.is_pid_used[pid_map.num_available_pids] = true;
	return pid_map.available_pids[pid_map.num_available_pids];
}

void release_pid(int pid)
{
	int index = pid - MIN_PID;
	if (index >= 0 && index < END_PID - MIN_PID &&
	    pid_map.is_pid_used[index]) {
		pid_map.is_pid_used[index] = false;
		pid_map.available_pids[pid_map.num_available_pids] = pid;
		pid_map.num_available_pids++;
	}
}

int main()
{
	allocate_map();
	int pid1 = allocate_pid();
	int pid2 = allocate_pid();
	assert(pid1 != pid2);
	assert(pid1 >= MIN_PID && pid1 <= MAX_PID);
	assert(pid2 >= MIN_PID && pid2 <= MAX_PID);

	for (int i = 0; i <= MAX_PID - MIN_PID - 2; i++) {
		int pid = allocate_pid();
		assert(pid >= MIN_PID && pid <= MAX_PID);
	}
	assert(allocate_pid() == -1);
	release_pid(MIN_PID + 3);
	assert(allocate_pid() == MIN_PID + 3);
	release_pid(MIN_PID + 30);
	assert(allocate_pid() == MIN_PID + 30);

	return 0;
}
