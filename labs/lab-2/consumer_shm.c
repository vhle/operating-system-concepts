#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main()
{
	const int SIZE = 4096;
	const char *name = "OS";
	int fd;
	char *ptr;

	if ((fd = shm_open(name, O_RDONLY, 0666)) == -1) {
		fprintf(stderr, "Consumer: Error opening shared memory");
		exit(1);
	}

	ptr = (char *)mmap(0, SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED) {
		fprintf(stderr, "Consumer: memory map failed: %d\n", errno);
		exit(1);
	}
	printf("%s\n", (char *)ptr);
	if (shm_unlink(name) == -1) {
		fprintf(stderr, "Unlink error.\n");
		exit(1);
	}
	return 0;
}
