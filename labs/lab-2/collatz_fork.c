#include <sched.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

void help(char *program_name)
{
	printf("Usage:\n\t%s <positive integer>.\n", program_name);
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		help(argv[0]);
		exit(1);
	}
	int n = atoi(argv[1]);
	if (n <= 0) {
		help(argv[0]);
		exit(1);
	}

	pid_t child_pid = fork();
	if (child_pid == 0) {
		pid_t pid = getpid();
		while (1) {
			printf("%d (child): %d\n", pid, n);
			if (n <= 1) {
				break;
			}
			n = (n & 1) ? (3 * n + 1) : (n >> 1);
		};
	} else {
		wait(NULL);
		printf("%d (parent): Done.\n", getpid());
	}
	return 0;
}
