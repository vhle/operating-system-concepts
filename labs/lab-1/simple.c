/** A simple kernel module */

#include <linux/gcd.h>
#include <linux/hash.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>
#include <linux/module.h>

static ulong start_jiffies = 0;

/** Function to be called when the module is loaded */
int simple_init(void) {
  printk(KERN_INFO "Loading Kernel Module\n");

  /** Access a value defined by kernel source */
  printk(KERN_INFO "GOLDEN_RATIO_PRIME: %llu\n", GOLDEN_RATIO_PRIME);

  printk(KERN_INFO "Timer interrupt frequency: %d\n", HZ);
  start_jiffies = jiffies;
  printk(KERN_INFO "jiffies: %lu\n", start_jiffies);
  return 0;
}

void simple_exit(void) {
  ulong end_jiffies = jiffies;
  printk(KERN_INFO "Removing Kernel Module\n");

  /** Access a function defined by kernel source */
  printk(KERN_INFO "gcd(3300, 24) = %lu\n", gcd(3300, 24));
  printk(KERN_INFO "jiffies: %lu\n", end_jiffies);
  printk(KERN_INFO "Time elapsed since the module was loaded then removed %lu\n", (end_jiffies - start_jiffies) / HZ);
}

/** Macros for registering module entry and exit functions */
module_init(simple_init);
module_exit(simple_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("");
