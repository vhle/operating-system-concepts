#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

#define MODULE_NAME "jiffies"
#define BUFFER_SIZE 128

ssize_t calculate_time_elapsed(struct file *file, char __user *usr_buf,
			       size_t count, loff_t *pos);

static struct file_operations calculate_elapsed_time_ops = {
	.owner = THIS_MODULE,
	.read = calculate_time_elapsed,
};

int jiffies_init(void)
{
	printk(KERN_INFO "jiffies_init");
	proc_create(MODULE_NAME, 0666, NULL, &calculate_elapsed_time_ops);
	return 0;
}

void jiffies_exit(void)
{
	printk(KERN_INFO "jiffies_exit");
	remove_proc_entry(MODULE_NAME, NULL);
}

ssize_t calculate_time_elapsed(struct file *file, char __user *usr_buf,
			       size_t count, loff_t *pos)
{
	int rv = 0;
	char buffer[BUFFER_SIZE];

	static int completed = 0;
	if (completed) {
		completed = 0;
		return 0;
	}
	completed = 1;

	rv = sprintf(buffer, "%lu\n", jiffies);
	copy_to_user(usr_buf, buffer, rv);

	return rv;
}

module_init(jiffies_init);
module_exit(jiffies_exit);

MODULE_LICENSE("GPL");
