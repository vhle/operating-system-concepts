#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

#define BUFFER_SIZE 128
const char PROC_NAME[] = "hello";


ssize_t proc_read(struct file* file, char __user *usr_buf, size_t count, loff_t *pos);

static struct file_operations proc_ops = {.owner = THIS_MODULE, .read = proc_read};


int proc_init(void) {
  /* Create /proc/hello */
  proc_create(PROC_NAME, 0666, NULL, &proc_ops);
  return 0;
}

void proc_exit(void) {
  /* Remove /proc/hello */
  remove_proc_entry(PROC_NAME, NULL);
}


ssize_t proc_read(struct file* file, char __user *usr_buf, size_t count, loff_t *pos) {
  int rv = 0;
  char buffer[BUFFER_SIZE];
  static int completed = 0;

  if (completed) {
    completed = 0;
    return 0;
  }

  completed = 1;
  rv = sprintf(buffer, "Hello, World!\n");

  /* Copy kernel space buffer to user space buffer usr_buf */
  copy_to_user(usr_buf, buffer, rv);

  return rv;
}

module_init(proc_init);
module_exit(proc_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Hello module");
MODULE_AUTHOR("VHL");

