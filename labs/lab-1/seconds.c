#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

#define MODULE_NAME "seconds"
#define BUFFER_SIZE 128

ssize_t calculate_elapsed_time(struct file *file, char __user *usr_buf,
			       size_t count, loff_t *pos);
static ulong jiffies_at_module_init;
static struct file_operations calculate_elapsed_time_ops = {
	.owner = THIS_MODULE,
	.read = calculate_elapsed_time,
};

int seconds_init(void)
{
	jiffies_at_module_init = jiffies;
	printk(KERN_INFO "jiffies_init");
	proc_create(MODULE_NAME, 0666, NULL, &calculate_elapsed_time_ops);
	return 0;
}

void seconds_exit(void)
{
	printk(KERN_INFO "jiffies_exit");
	remove_proc_entry(MODULE_NAME, NULL);
}

ssize_t calculate_elapsed_time(struct file *file, char __user *usr_buf,
			       size_t count, loff_t *pos)
{
	int rv = 0;
	char buffer[BUFFER_SIZE];
	ulong seconds_elapsed;

	static int completed = 0;
	if (completed) {
		completed = 0;
		return 0;
	}

	seconds_elapsed = (jiffies - jiffies_at_module_init) / HZ;
	rv = sprintf(buffer, "%lu\n", seconds_elapsed);
	copy_to_user(usr_buf, buffer, rv);

	completed = 1;
	return rv;
}

module_init(seconds_init);
module_exit(seconds_exit);

MODULE_LICENSE("GPL");
